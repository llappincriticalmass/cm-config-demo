package com.cm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class CriticalMassConfigApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(CriticalMassConfigApplication.class, args);
	}

}
